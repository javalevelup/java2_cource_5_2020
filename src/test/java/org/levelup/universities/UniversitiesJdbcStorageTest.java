package org.levelup.universities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.levelup.universities.jdbc.JdbcService;
import org.levelup.universities.jdbc.UniversitiesJdbcStorage;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UniversitiesJdbcStorageTest {

    private UniversitiesJdbcStorage jdbcStorage;
    // 2 variant
    @Mock
    private JdbcService jdbcService;
    @Mock
    private Connection connection;
    @Mock
    private PreparedStatement statement;

    @BeforeEach
    public void setupJdbcStorage() throws SQLException {
        MockitoAnnotations.openMocks(this);
        // 1 variant
        // jdbcService = Mockito.mock(JdbcService.class);
        jdbcStorage = Mockito.spy(new UniversitiesJdbcStorage(jdbcService));

        Mockito.when(jdbcService.openConnection()).thenReturn(connection);
        Mockito.when(connection.prepareStatement(ArgumentMatchers.anyString()))
                .thenReturn(statement);
    }

    @Test
    public void testCreateUniversity_whenDataIsValid_thenCreateUniversity() throws SQLException {
        // given
        String name = "name";
        String shortName = "short name";
        int foundationYear = 1994;

        Mockito.when(statement.executeUpdate()).thenReturn(10);

        // when
        jdbcStorage.createUniversity(name, shortName, foundationYear);

        // then
        Mockito.verify(statement, Mockito.times(2))
                .setString(ArgumentMatchers.anyInt(), ArgumentMatchers.anyString());
        Mockito.verify(statement).setInt(3, foundationYear);
        Mockito.verify(statement).executeUpdate();
        // Mockito.verify(statement, Mockito.times(0)).executeUpdate();
    }

    @Test
    public void testCreateUniversity_whenCouldNotOpenConnection_thenThrowException() throws SQLException {
        // given
        Mockito.when(jdbcService.openConnection()).thenThrow(SQLException.class);
        // Mockito.doThrow(SQLException.class).when(jdbcService).openConnection();
        // Mockito.doThrow(SQLException.class).when(statement).setNull(1,1);

        // where
        Assertions.assertThrows(RuntimeException.class, () ->
                jdbcStorage.createUniversity("name", "short name", 1000));
    }

}
