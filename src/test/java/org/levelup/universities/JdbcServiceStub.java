package org.levelup.universities;

import org.levelup.universities.jdbc.JdbcService;

import java.sql.Connection;
import java.sql.SQLException;

public class JdbcServiceStub extends JdbcService {

    @Override
    public Connection openConnection() throws SQLException {
        return new ConnectionStub();
    }

}
