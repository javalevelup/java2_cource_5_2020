package org.levelup.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

@SuppressWarnings("ALL")
public class StringUtilsTest {

    // isBlankShouldReturnTrueThenValueIsNull
    @Test
    public void testIsBlank_whenValueIsNull_thenReturnTrue() {
        // given
        // when
        boolean result = StringUtils.isBlank(null);
        // then
        Assertions.assertTrue(result, "Method isBlank should return TRUE when value is null");
    }

    @Test
    public void testIsBlank_whenValueIsEmptyString_thenReturnTrue() {
        // given
        String value = "";
        // when
        boolean result = StringUtils.isBlank(value);
        // then
        Assertions.assertTrue(result, "Method isBlank should return TRUE when value is an empty string");
    }

    @Test
    public void testIsBlank_whenValueConsistsOnlyFromWhitespaces_thenReturnTrue() {
        // given
        String value = "     ";
        // when
        boolean result = StringUtils.isBlank(value);
        // then
        Assertions.assertTrue(result, "Method isBlank should return TRUE when value constists only from whitespaces");
    }

    @Test
    public void testIsBlank_whenValueIsNotEmptyString_thenReturnFalse() {
        // given
        String value = "some string value";
        // when
        boolean result = StringUtils.isBlank(value);
        // then
        Assertions.assertFalse(result, "Method isBlank should return FALSE when value isn't empty string");
    }

}
