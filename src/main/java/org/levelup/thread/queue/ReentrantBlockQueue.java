package org.levelup.thread.queue;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantBlockQueue implements Queue {

    private final LinkedList<Runnable> tasks;
    private final int capacity; // сколько задач может находиться в очереди

    private final ReentrantLock lock;
    private final Condition emptyCondition;
    private final Condition fullCondition;

    public ReentrantBlockQueue(int capacity) {
        this.tasks = new LinkedList<>();
        this.capacity = capacity;

        this.lock = new ReentrantLock();
        this.emptyCondition = lock.newCondition();
        this.fullCondition = lock.newCondition();
    }

    @Override
    public void add(Runnable runnable) throws InterruptedException {
        lock.lock();
        try {
            while (tasks.size() == capacity) {
                fullCondition.await();
            }

            tasks.addLast(runnable);
            emptyCondition.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Runnable take() throws InterruptedException {
        lock.lock();
        try {
            while (tasks.isEmpty()) {
                emptyCondition.await();
            }

            Runnable task = tasks.poll();
            fullCondition.signal();
            return task;
        } finally {
            lock.unlock();
        }
    }

}
