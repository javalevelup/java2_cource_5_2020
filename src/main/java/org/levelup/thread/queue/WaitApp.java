package org.levelup.thread.queue;

public class WaitApp {

    public static void main(String[] args) throws InterruptedException {
        Object obj = new Object();
        obj.wait(); // throw new IllegalMonitorStateException
    }

}
