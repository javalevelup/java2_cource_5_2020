package org.levelup.thread.queue;

import java.util.LinkedList;

public class BlockingQueue implements Queue {

    private final LinkedList<Runnable> tasks;
    private final int capacity; // сколько задач может находиться в очереди

    private final Object fullQueue = new Object();
    private final Object emptyQueue = new Object();

    public BlockingQueue(int capacity) {
        this.tasks = new LinkedList<>();
        this.capacity = capacity;
    }

    @Override
    public void add(Runnable runnable) throws InterruptedException {
        // t1 взял монитор, очередь заполнена, t1 освободил монитор и ушел в wait
        synchronized (fullQueue) {
            while (tasks.size() == capacity) { // случай, когда очередь заполнена
                // wait(1000)
                // sleep(1000)
                fullQueue.wait(); //
            }
        }

        synchronized (emptyQueue) {
            tasks.addLast(runnable); // у нас в очереди может не быть места
            emptyQueue.notify(); // разбудили потоки, которые ожидали добавления задачи в очередь
        }
    }

    @Override
    public Runnable take() throws InterruptedException {
        synchronized (emptyQueue) {
            while (tasks.isEmpty()) {
                emptyQueue.wait();
            }
        }

        synchronized (fullQueue) {
            Runnable task = tasks.poll(); // null если очередь пуста
            fullQueue.notify();
            return task;
        }
    }

}
