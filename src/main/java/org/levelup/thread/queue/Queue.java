package org.levelup.thread.queue;

public interface Queue {

    // добавить в конец очереди
    void add(Runnable runnable) throws InterruptedException;

    // забрать из начала очереди
    Runnable take() throws InterruptedException;

}
