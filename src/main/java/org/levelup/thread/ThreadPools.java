package org.levelup.thread;

import java.time.LocalDateTime;
import java.util.Timer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ThreadPools {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        // Пул из одного потока
        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();

        Future<LocalDateTime> future = singleThreadExecutor.submit(() -> {
            return LocalDateTime.now();
        });

        LocalDateTime now = future.get();
        System.out.println(now);


        ExecutorService tenThreadsPool = Executors.newFixedThreadPool(10);
        tenThreadsPool.shutdown();

        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        cachedThreadPool.shutdown();

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(4);
        scheduledExecutorService.scheduleAtFixedRate(() -> System.out.println("Hello"), 10, 1, TimeUnit.SECONDS);
        scheduledExecutorService.scheduleWithFixedDelay(() -> System.out.println("Hello"), 10, 1, TimeUnit.SECONDS);

        scheduledExecutorService.shutdown();

        singleThreadExecutor.shutdown();
    }

}
