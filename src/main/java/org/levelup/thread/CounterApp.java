package org.levelup.thread;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import javax.swing.plaf.basic.BasicTreeUI;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

// biased - reentrantlock
// thin - reentrantlock, synchronized: thin содержит значение, которое обознает, какой поток держит блокировку, CAS
// fat  - reentrantlock, synchronized:
public class CounterApp {

    public static void main(String[] args) throws InterruptedException {

        Counter counter = new Counter();

        Thread t1 = new Thread(new CountWorker(counter), "t1");
        Thread t2 = new Thread(new CountWorker(counter), "t2");
        Thread t3 = new Thread(new CountWorker(counter), "t3");

        t1.start();
        t2.start();
        t3.start();

        t1.join();
        t2.join();
        t3.join();

        System.out.println("Total count " + counter.getValue());
    }

    static class Counter {

        private final Object mutex = new Object();

        private long value;

        // public synchronized void increment() {
        public void increment() {
            // t1 взял монитор, t2 и t3 ожидают
            // t1 отпустил монитор, t2 и t3 проснулись и пытаются взять монитор
            // t3 взял монитор, t2 ожидает
            synchronized (this) {
                // t1 read value -> 10
                // t2 read value -> 10
                // t1 write value -> 11
                // t2 write value -> 11
                value++;
            }
        }

        public long incrementAndGet() {
            synchronized (mutex) {
                increment();
                return value;
            }
        }

        public long getValue() {
            return value;
        }

    }

    static class ReentrantCounter {

        private ReentrantLock reentrantLock = new ReentrantLock();
        private long value;

        public long incrementAndGet() {
            reentrantLock.tryLock();
//            Condition condition = reentrantLock.newCondition();
//            condition.
            value++;
            return get();
        }

        public long get() {
            long val = value;
            reentrantLock.unlock();
            return val;
        }

    }

    static class NonBlockingCounter {

        private AtomicLong value = new AtomicLong();

        // CAS - compare and set
        public void increment() {
            value.incrementAndGet();
        }

    }

    @AllArgsConstructor
    static class CountWorker implements Runnable {

        private Counter counter;

        @Override
        @SneakyThrows
        public void run() {
            for (int i = 0; i < 20; i++) {
                counter.increment();
                System.out.println(Thread.currentThread().getName() + " " + counter.getValue());
                Thread.sleep(100);
            }
        }

    }

}
