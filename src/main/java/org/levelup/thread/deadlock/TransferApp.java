package org.levelup.thread.deadlock;

public class TransferApp {

    public static void main(String[] args) throws InterruptedException {
        Account a = new Account("42352-234123", 5443.3d);
        Account b = new Account("3405-32421", 5667.65d);

        TransferService transferService = new TransferService();

        for (int i = 0; i < 1000; i++) {

            new Thread(new Transfer(a, b, transferService, 540)).start();
            new Thread(new Transfer(b, a, transferService, 2384)).start();

            // Thread.sleep(40);
            System.out.println(i);
        }

        System.out.println(a.getAmount());
        System.out.println(b.getAmount());

    }

}
