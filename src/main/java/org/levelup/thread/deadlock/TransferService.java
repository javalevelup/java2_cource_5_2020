package org.levelup.thread.deadlock;

public class TransferService {

    // a -> b, c -> d
    // c -> b
    // Transfer money from Account a to Account b
    public void transferMoney(Account a, Account b, double amount) {
        Account first = a.getAccountId().compareTo(b.getAccountId()) >= 0 ? a : b;
        Account second = first == a ? b : a;

//        synchronized (this) {
        synchronized (first) { //
            synchronized (second) {
                a.setAmount(a.getAmount() - amount);
                b.setAmount(b.getAmount() + amount);
            }
        }
    }

}
