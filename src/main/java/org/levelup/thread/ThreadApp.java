package org.levelup.thread;

public class ThreadApp {

    public static void main(String[] args) throws InterruptedException {
        // extends Thread
        // implement Runnable
        // implement Callable

        FirstThread t = new FirstThread();
        t.setName("first-thread");
        t.start(); // дает команду на создание и запуск потока

        t.join(); // main должен ожидать пока поток t не завершит свое выполнение

        Thread runnable = new Thread(new PrintHelloWorldRunnable(), "runnable-thread");
        runnable.start();
        runnable.join();

        // runnable.stop(); // kill -9 pid

        System.out.println("End of " + Thread.currentThread().getName());
    }

    static class FirstThread extends Thread {

        @Override
        public void run() {
            // runnable.join();
            // start point of thread execution
            for (int i = 0; i < 10; i++) {
                // System.out.println(Thread.currentThread().getName() + " " + i);
                System.out.println(getName() + " " + i);
            }
        }

    }

    static class PrintHelloWorldRunnable implements Runnable {

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + " hello world!");
            }
        }

    }

}
