package org.levelup.thread.interruption;

public class ThreadInterruption {

    public static void main(String[] args) throws InterruptedException {

        Thread t = new Thread(new BusyWorker(), "Busy-Worker");
        t.start();

        // t.stop(); <- kill -9 pid
        // kill -3 pid


        Thread.sleep(3000);
        t.interrupt(); // interrupt this thread

        Thread.sleep(4000);
        t.interrupt();

    }

    static class BusyWorker implements Runnable {

        @Override
        public void run() {
            int count = 0;

            while (!Thread.currentThread().isInterrupted()) {
                try {
                    // Thread.interrupted(); - true/false, но если флаг был true, то он тогда переведет его в false
                    // Thread.currentThread().isInterrupted() - true/false возвращает флаг прерывания
                    System.out.println("Work-work-work");
                    count++;
                    Thread.sleep(1000);
                } catch (InterruptedException exc) {
                    Thread.currentThread().interrupt();
                    if (count < 6 && Thread.interrupted()) {
                        System.out.println("Очищаем флаг interruption");
                    }
                    System.out.println("Поток спал, но его прервали");
                }
            }
            System.out.println(count);
        }

    }

}
