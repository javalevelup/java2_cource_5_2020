package org.levelup.thread;

// OutOfMemoryError: cannot create thread
public class StringReverse {

    private final Object mutex = new Object();
    private String value;

    // abc -> cba
    public String reverse() {
        synchronized (mutex) {
            StringBuilder sb = new StringBuilder(value);
            value = sb.reverse().toString();
            return value;
        }
    }

    public void setValue(String value) {
        synchronized (mutex) {
            this.value = value;
        }
    }

}
