package org.levelup.reflection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BookService {

    private int getBooksMethodInvokedCounter = 0;

    public List<String> getBooks() {
        long startTime = System.nanoTime();
        getBooksMethodInvokedCounter++;
        System.out.println("getBooks() method is invoked");
        List<String> books = new ArrayList<>(Arrays.asList("Book1", "Book2", "Book3"));
        System.out.println("Execution time: " + (System.nanoTime() - startTime));
        return books;
    }

}
