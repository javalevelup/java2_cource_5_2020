package org.levelup.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

@SuppressWarnings("ALL")
public class Reflection {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException,
            InvocationTargetException, InstantiationException {
        // 1 var: есть объект нужного класса
        Phone phoneObject = new Phone();
        Class<?> phoneClassObject = phoneObject.getClass(); // ? - wildcard

        // 2 var: когда нет объекта нужного класса
        Class<?> phClassObject = Phone.class;

        System.out.println("Class name: " + phoneClassObject.getName());
        System.out.println("Two class object are equal each other: " + (phoneClassObject == phClassObject));

        Class<?> intClassObject = int.class;
        System.out.println("isPrimitive(): " + intClassObject.isPrimitive());

        Field[] declaredField = phClassObject.getDeclaredFields(); // getFields() - возвращает только публичные поля

        Field modelField = phClassObject.getDeclaredField("model");
        System.out.println();
        System.out.println("Field name: " + modelField.getName());
        System.out.println("Field type: " + modelField.getType().getName()); // getType() возвращает Class<?>

        Phone samsung = new Phone("Samsung 10", 4, 2.1);
        System.out.println();
        modelField.setAccessible(true);
        System.out.println("Field value: " + modelField.get(samsung)); // get - получить значения поля (Field) у объекта samsung

        modelField.set(samsung, "Samsung 11"); // set -> установить новое значение поля у объекта samsung
        System.out.println("Field value after changes: " + samsung.getModel());

        Constructor<?> constructor = phClassObject.getDeclaredConstructor(double.class);
        constructor.setAccessible(true);
        Phone createdWithPrivateConstructor = (Phone) constructor
                .newInstance(2.3d);
        System.out.println("cpu from private consturctor: " + createdWithPrivateConstructor.getCpu());

    }

}
