package org.levelup.reflection;

import org.levelup.universities.jdbc.App;

public class ClassLoaders {

    public static void main(String[] args) throws ClassNotFoundException {
        String s = "";
        ClassLoader load = s.getClass().getClassLoader();
        System.out.println(load);

        ClassLoader appClassloader = App.class.getClassLoader();
        System.out.println(appClassloader);

        Class.forName("org.postgresql.Driver"); // << for JDBC version < 4
    }

}
