package org.levelup.reflection.annotation;

public class Book {

    @RandomInt(min = 3000, max = 6000)
    private int bookNumber;
    @RandomInt(min = 10, max = 2000)
    private int pagesCount;

    private int symbolCount;

    @Override
    public String toString() {
        return "Book{" +
                "bookNumber=" + bookNumber +
                ", pagesCount=" + pagesCount +
                ", symbolCount=" + symbolCount +
                '}';
    }

}
