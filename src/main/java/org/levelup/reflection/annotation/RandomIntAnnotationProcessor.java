package org.levelup.reflection.annotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Random;

public class RandomIntAnnotationProcessor {

    public <T> T createAndProcess(Class<T> tClass) {
        try {
            Constructor<T> constructor = tClass.getDeclaredConstructor(); // коструктор без параметров
            T instance = constructor.newInstance(); // создали объект T через Reflection

            // Найти все поля, помеченные аннотацией RandomInt
            Field[] fields = tClass.getDeclaredFields();
            for (Field field : fields) {
                // проверяем, что у поля есть аннотация RandomInt
                RandomInt annotation = field.getAnnotation(RandomInt.class); // RandomInt.class - получить объект класса Class у аннотации RandomInt
                if (annotation != null) {
                    int randomInt = generateRandomInteger(annotation.min(), annotation.max());
                    // записать значение в поле
                    field.setAccessible(true);
                    field.set(instance, randomInt);
                }
            }

            return instance;
        } catch (Exception exc) {
            throw new RuntimeException(exc);
        }
    }

    private int generateRandomInteger(int min, int max) {
        return new Random().nextInt(max - min) + min; // [min, max) - левая граница включительно, правая нет
    }

}
