package org.levelup.utils;

public class StringUtils {

    public static boolean isBlank(String value) {
        // true - если строка является null или пустая или состоит из одних пробелов
        // true: null, "", "    "
        // trim() - убирает пробелы в начале строке и в конце: "   343ert ".trim() -> "343ert"
        return value == null || value.trim().isEmpty();
    }

}
