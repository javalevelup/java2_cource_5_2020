package org.levelup.universities.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UniversitiesJdbcStorage {

    private JdbcService jdbcService;

    public UniversitiesJdbcStorage(JdbcService jdbcService) {
        this.jdbcService = jdbcService;
    }

    public void createUniversity(String name, String shortName, int foundationYear) {
        if (foundationYear < 0) {
            throw new IllegalArgumentException("Foundation year must be positive");
        }

        // Создаем новую запись в таблице universities
        try (Connection connection = jdbcService.openConnection()) {
            // Statement, PreparedStatement, CallableStatement
            PreparedStatement statement = connection
                    .prepareStatement("insert into university (name, short_name, foundation_year) values (?, ?, ?)");

            statement.setString(1, name);
            statement.setString(2, shortName);
            statement.setInt(3, foundationYear);

            // возвращает количество строк, которые были изменены
            int rowsAffected = statement.executeUpdate(); // << выполняет (используется для) insert/update/delete
            System.out.println("Rows affected: " + rowsAffected);

        } catch (SQLException exc) {
            System.out.println("Couldn't open new connection: " + exc.getMessage());
            throw new RuntimeException(exc);
        }

    }

    public void displayUniversities() {
        try (Connection connection = jdbcService.openConnection()) {
            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery("select * from university"); // << выполняет (используется для) select
            while (rs.next()) {
                // rs <- указатель на текущую строку в цикле
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String shortName = rs.getString("short_name");
                int foundationYear = rs.getInt("foundation_year");

                // Integer id, Integer foundationYear
                // System.out.println(String.join(" ", id, name, shortName, foundationYear));

                System.out.println(id + " " + name + " " + shortName + " " + foundationYear);
            }

        } catch (SQLException exc) {
            System.out.println("Couldn't open new connection: " + exc.getMessage());
            throw new RuntimeException(exc);
        }
    }

    // Object..., Integer... - vararg -> Object[], Integer[]
    //  findBySql("");
    //  findBySql("", 54);
    //  findBySql("", 54, "some string", new Object());
    //  findBySql("", new Object[] {});
    public void findBySql(String sql, Object... args) {
        try (Connection connection = jdbcService.openConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql); // "select * from .... where ... ?"

            int parameterIndex = 1;
            for (Object argument : args) {
                statement.setObject(parameterIndex++, argument);
            }

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String shortName = rs.getString("short_name");
                int foundationYear = rs.getInt("foundation_year");

                System.out.println(id + " " + name + " " + shortName + " " + foundationYear);
            }

        } catch (SQLException exc) {
            System.out.println("Couldn't open new connection: " + exc.getMessage());
            throw new RuntimeException(exc);
        }
    }

}
