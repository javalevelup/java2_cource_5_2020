package org.levelup.universities.jdbc;

public class App {

    public static void main(String[] args) {
        UniversitiesJdbcStorage storage = new UniversitiesJdbcStorage(new JdbcService());
        storage.findBySql("select * from university where foundation_year > ?", 1900);
        System.out.println();
        storage.findBySql("select * from university where short_name like ?", "СПб%");

        //storage.displayUniversities();
//        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
//            System.out.println("Enter university name: ");
//            String name = reader.readLine();
//
//            System.out.println("Enter university short name: ");
//            String shortName = reader.readLine();
//
//            System.out.println("Enter foundation year: ");
//            int foundationYear = Integer.parseInt(reader.readLine());
//
//            storage.createUniversity(name, shortName, foundationYear);
//
//        } catch (IOException exc) {
//            throw new RuntimeException(exc);
//        }

    }

}
