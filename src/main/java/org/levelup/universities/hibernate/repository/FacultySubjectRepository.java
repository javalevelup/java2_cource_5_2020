package org.levelup.universities.hibernate.repository;

public interface FacultySubjectRepository {

    void weaveSubjectAndFaculty(Integer subjectId, Integer facultyId);

}
