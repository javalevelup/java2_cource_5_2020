package org.levelup.universities.hibernate.repository;

// 1 layer - Controllers
// 2 layer - Services
// 3 layer - DAO (Repositories)

import org.levelup.universities.hibernate.domain.FacultyEntity;

// FacultyDAO (FacultyDao)
// DAO - Data Access Object - объект доступа к данным
//  classical dao:
//      - CRUD operations: create, read (readAll, readByID), update, delete
//      - no methods like: findBy<FIELD>, check<Smth>
// Repository - DAO + методы поиска/проверки и другие (find, check, etc.)
public interface FacultyRepository {

    FacultyEntity createFaculty(Integer universityId, Integer facultyId, String name);

    FacultyEntity getById(Integer facultyId);

    FacultyEntity loadById(Integer facultyId);

}
