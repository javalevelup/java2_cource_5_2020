package org.levelup.universities.hibernate.repository;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.universities.hibernate.domain.FacultyEntity;
import org.levelup.universities.hibernate.domain.UniversityEntity;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

// @RequiredArgsConstructor // создает конструктор только с final полями
public class HibernateUniversityRepository extends AbstractRepository implements UniversityRepository {

    public HibernateUniversityRepository(SessionFactory factory) {
        super(factory);
    }

    @Override
    public UniversityEntity createUniversity(String name, String shortName, int foundationYear) {
        try (Session s = factory.openSession()) { // открываем сессию (соединение к базе)
            Transaction transaction = s.beginTransaction(); // начали транзакцию

            UniversityEntity university = new UniversityEntity(name, shortName, foundationYear);
            // persist автоматически сделаем setId() у объекта university
            s.persist(university); // делаем insert в таблицу university
            // Integer id = (Integer) s.save(university);
            // UniversityEntity u = (UniversityEntity) s.merge(university);

            transaction.commit(); // завершаем транзакцию успешно (фиксируем все изменения, сделанные в транзакции)

            return university;
        }
    }

    @Override
    public UniversityEntity createUniversity(String name, String shortName, int foundationYear, int facultyId, String facultyName) {
        try (Session s = factory.openSession()) { // открываем сессию (соединение к базе)
            Transaction transaction = s.beginTransaction(); // начали транзакцию

            UniversityEntity university = new UniversityEntity(name, shortName, foundationYear);

            FacultyEntity faculty = new FacultyEntity();
            faculty.setFacultyId(facultyId);
            faculty.setName(facultyName);
            faculty.setUniversity(university);

            s.persist(university); // делаем insert в таблицу university
            s.persist(faculty);

            transaction.commit(); // завершаем транзакцию успешно (фиксируем все изменения, сделанные в транзакции)

            return university;
        }
    }

    @Override
    public UniversityEntity findById(int universityId) {
//        try (Session s = factory.openSession()) {
//            // s.get(UniversityEntity.class, universityId);
//            UniversityEntity university = s.createQuery("from UniversityEntity where id = :id", UniversityEntity.class)
//                    .setParameter("id", universityId)
//                    .getSingleResult();  // getResultList() вернет List<UniversityEntity>
//            // System.out.println(university);
//            List<FacultyEntity> faculties = university.getFaculties();
//            System.out.println(faculties);
//            return university;
//        }
        // return run(session -> session.createQuery(....).getSingleResult());
        return run(s ->
                s.createQuery("from UniversityEntity where id = :id", UniversityEntity.class)
                        .setParameter("id", universityId)
                        .getSingleResult()
        );
    }

    @Override
    public Collection<UniversityEntity> findAll() {
        // return run(session -> session.createQuery(....).getResultList());
//        SessionExecutor<Collection<UniversityEntity>> findAllUniversitiesExecutor = new SessionExecutor<Collection<UniversityEntity>>() {
//            @Override
//            public Collection<UniversityEntity> execute(Session s) {
//                return s.createQuery("from UniversityEntity", UniversityEntity.class)
//                        .getResultList();
//            }
//        };

        return run(s -> s.createQuery("from UniversityEntity", UniversityEntity.class).getResultList());
    }

}
