package org.levelup.universities.hibernate.repository;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.universities.hibernate.domain.FacultyEntity;
import org.levelup.universities.hibernate.domain.SubjectEntity;

@RequiredArgsConstructor
public class HibernateFacultySubjectRepository implements FacultySubjectRepository {

    private final SessionFactory factory;

    //        runInTransaction(session -> {
    //            Person loadedPerson = (Person) session.merge(person);
    //            Bank bank = session.get(Bank.class, bankId);
    //
    //            bank.getBankPersons().add(loadedPerson);
    //            person.getBanks().add(bank);
    //
    //            session.persist(bank);
    //        });
    @Override
    public void weaveSubjectAndFaculty(Integer subjectId, Integer facultyId) {
        try (Session s = factory.openSession()) {
            Transaction t = s.beginTransaction();
            SubjectEntity subject = s.get(SubjectEntity.class, subjectId);
            FacultyEntity faculty = s.get(FacultyEntity.class, facultyId);

            subject.getFaculties().add(faculty);
            faculty.getSubjects().add(subject);

            t.commit();
        }
    }

}
