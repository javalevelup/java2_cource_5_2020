package org.levelup.universities.hibernate.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "subject_info")
public class SubjectInfoEntity {

    @Id
    @Column(name = "subject_id")
    private Integer subjectId;
    private String description;
    @Column(name = "room_number")
    private Integer roomNumber;

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn // @JoinColumn(name = "subject_id")
    private SubjectEntity subject;

}
