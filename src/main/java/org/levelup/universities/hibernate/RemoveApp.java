package org.levelup.universities.hibernate;

import org.hibernate.SessionFactory;
import org.levelup.universities.hibernate.config.HibernateConfiguration;
import org.levelup.universities.hibernate.repository.HibernateSubjectRepository;
import org.levelup.universities.hibernate.repository.SubjectRepository;

public class RemoveApp {

    public static void main(String[] args) {

        SessionFactory factory = HibernateConfiguration.getFactory();
        SubjectRepository subjectRepository = new HibernateSubjectRepository(factory);

        subjectRepository.removeSubject(40);

        factory.close();
    }

}
