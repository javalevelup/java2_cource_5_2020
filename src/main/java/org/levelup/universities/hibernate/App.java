package org.levelup.universities.hibernate;

import org.hibernate.SessionFactory;
import org.levelup.universities.hibernate.config.HibernateConfiguration;
import org.levelup.universities.hibernate.domain.UniversityEntity;
import org.levelup.universities.hibernate.repository.HibernateUniversityRepository;
import org.levelup.universities.hibernate.repository.UniversityRepository;

public class App {

    public static void main(String[] args) {
        SessionFactory factory = HibernateConfiguration.getFactory();

        UniversityRepository universityRepository = new HibernateUniversityRepository(factory);
//        UniversityEntity university = universityRepository
//                .createUniversity("Лениградский ЭлектроТехнический Интститут 2", "ЛЭТИ 2", 1944, 2454, "Инженерный");

        // System.out.println("ID ЛЭТИ: " + university.getId());

         UniversityEntity secondUniversity = universityRepository.findById(102);
        System.out.println(secondUniversity.toString());
        // System.out.println(secondUniversity.getName() + " " + secondUniversity.getShortName() + " " + secondUniversity.getFoundationYear());

        factory.close();
    }

}
