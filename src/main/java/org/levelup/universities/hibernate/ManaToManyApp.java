package org.levelup.universities.hibernate;

import org.hibernate.SessionFactory;
import org.levelup.universities.hibernate.config.HibernateConfiguration;
import org.levelup.universities.hibernate.domain.FacultyEntity;
import org.levelup.universities.hibernate.repository.*;

public class ManaToManyApp {

    public static void main(String[] args) {
        SessionFactory factory = HibernateConfiguration.getFactory();

        FacultyRepository facultyRepository = new HibernateFacultyRepository(factory);
        SubjectRepository subjectRepository = new HibernateSubjectRepository(factory);
        FacultySubjectRepository fsRepository = new HibernateFacultySubjectRepository(factory);
//
//        FacultyEntity faculty = facultyRepository.createFaculty(102, 234, "Экономический факультет");
//        subjectRepository.createSubject(1, "Экономика", 72);
//        subjectRepository.createSubject(2, "Физика", 72);
//        subjectRepository.createSubject(3, "Высшая математика", 300);

        fsRepository.weaveSubjectAndFaculty(1, 234);
        fsRepository.weaveSubjectAndFaculty(2, 234);
        fsRepository.weaveSubjectAndFaculty(3, 234);

        factory.close();
    }

}
