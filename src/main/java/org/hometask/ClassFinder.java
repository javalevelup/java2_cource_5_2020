package org.hometask;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
public class ClassFinder {

    public Collection<Class<?>> findAnnotatedClasses(String packagePath) {
        //
        String rootPath = "src/main/java/";
        // packagePath = "org.hometask..."
        // src/main/java/org/hometask/...
        File root = new File(rootPath + packagePath.replace(".", "/"));
        if (!root.exists()) {
            throw new RuntimeException("Package with such name doesn't exist");
        }

        Collection<File> files = findFilesInFolder(root);

        System.out.println("List of all files");
        files.forEach(file -> System.out.println(file.getAbsolutePath()));

        Collection<String> classNames = files.stream()
                .map(file -> file.getAbsolutePath()) // Stream<String> - данные о путях файлов
                .filter(filepath -> filepath.contains(".java"))
                .map(filepath -> {
                    int startIndex = filepath.indexOf("/java/") + 6;
                    int endIndex = filepath.indexOf(".java");
                    // /Users/dmitrijprocko/IdeaProjects/java_2_course_4_2020/src/main/java/org/hometask/reflect/A.java
                    // org/hometask/reflect/A
                    return filepath.substring(startIndex, endIndex);
                })
                .map(filepath -> filepath.replace("/", ".")) // Stream<String> - список полных имен классов в виде строк
                .collect(Collectors.toList());

        System.out.println("List of class \\names\\");
        classNames.forEach(className -> System.out.println(className));

        return classNames.stream()
                .map(className -> loadClass(className))
                .filter(clazz -> isClassAnnotated(clazz))
                .collect(Collectors.toList());

    }

    // Мы ищем все файлы в папке (рекурсивно - мы найдем все файлы в папке и в ее подпапках)
    private Collection<File> findFilesInFolder(File folder) {
        Collection<File> files = new ArrayList<>();
        File[] filesInFolder = folder.listFiles(); // << список всех файлов и папок в folder

        if (filesInFolder == null) {
            return files;
        }

        for (File file : filesInFolder) {
            if (file.isDirectory()) { // если file является папкой
                // то тогда должны искать файлы в этой папке
                files.addAll(findFilesInFolder(file));
            } else {
                files.add(file);
            }
        }

        return files;
    }

    // Class<Object>, Class
    private Class<?> loadClass(String className) { // wildcard
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Couldn't load class by name", e);
        }
    }

    private boolean isClassAnnotated(Class<?> clazz) {
        // clazz.getAnnotation()
        return clazz.isAnnotationPresent(ReflectClass.class);
    }

//    private void invokePrintCollection() {
//        Collection<Integer> integers = new ArrayList<>();
//        printCollection(integers);
//    }
//
//    private void printCollection(Collection<?> collection) {
//        // collection.add(1343);
//        // Iterator<?> iterator = collection.iterator();
//        // print
//    }

}
